
\subsection{Einführung und Definitionen}

\begin{defi}{Terminologie}
  Beschreibt alle Begriffe innerhalb einer Fachsprache.
  \tcblower
  \begin{center}
    \enquote{Fachwortschatz}
  \end{center}
\end{defi}

\begin{defi}{Nomenklatur}
  \textit{Verbindliche} Sammlung von Fachbegriffen aus einer Terminologie für einen bestimmten Fachbereich.
  \tcblower
  \[
    \text{Nomenklatur } \subseteq \text{ Terminologie} 
  \]
\end{defi}

\begin{defi}{Kontrolliertes Vokabular}
  Sammlung von Begriffen, die einem Konzept \textit{eindeutig} zugewiesen sind.
  Es gibt keine \textit{Homonyme} (ein Wort, welches für mehrere Sachverhalte steht).
\end{defi}

\begin{defi}{Thesaurus}
  Ein Kontrolliertes Vokabular, welches Synonyme von Begriffen kenntlich macht.
\end{defi}

\begin{defi}{Klassifikation}
  Sammlung abstrakter Begriffe, welche hierarchisch in Klassen eingeteilt werden können, abhängig von signifikanten, übereinstimmenden Merkmalen. Dabei gehört jeder Begriff genau einer Klasse an. Klassen können eine oder mehrere Unterklassen haben. 
  \tcblower
  Darauf aufbauend können \textit{Monohierarchische Klassifikationen}, in denen alle Begriffe \textit{genau eine} Oberklasse haben, und \textit{Polyhierarchische Klassifikationen}, in denen Begriffe mehrere Oberklassen haben können, definiert werden. 
\end{defi}

Der Unterschied zwischen Klassifikation und Terminologie (bzw. Nomenklatur) kann wie folgt verbildlicht werden:

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{img/03-nomeklatur-klassifikation.png}
  \caption{Unterschied Nomenklatur und Klassifikation}
  \label{img-03-nomeklatur-klassifikation}
\end{figure}

Eine Terminologie beschreibt einen einzelnen Gegenstand, unabhängig von seiner Kategorie; hier ein \enquote{weißer Gürtel}. 
Eine Klassifikation hingegen spezifiziert neben dem Gegenstand (bzw. dem Sachverhalt) die zugehörige Kategorie; hier \enquote{Kleinzeug}, sowie \enquote{Kleinzeug - Gürtel}. 

\begin{defi}{Ontologie}
  Ontologien beschreiben den Zusammenhang zwischen Bereichen und/oder Kategorien. Meist sind sie bildlich dargestellt. 
\end{defi}

Im Gegensatz zu Klassifikation können keine Vererbungseigenschaften dargestellt werden. 
Ontologien verbinden im Allgemeinen mehrere Klassifikationen miteinander.

\subsection{Terminologien in der Medizin} 

In der Anatomie sind mehrere Terminologien eingeführt worden:
\begin{enumerate}
  \item Basler Nomina Anatomica
  \item Jenaer Nomina Anatomica
  \item Pariser Nomina Anatomica
  \item Terminologica Anatomica
\end{enumerate}
Diese Beschreiben den Körper und seine Bestandteile in 3 Ebenen:
Transversalebene (von oben nach unten), Frontalebene (von vorne nach hinten), Sagittalebene (von links nach rechts).

\attention{LOINC, Logical Observation Identifier Names and Codes}, ist eine Standardterminologie, welche im Original Laborparameter definiert. 
Es wurde erweitert und enthält nun auch klinische Aspekte. 

 \attention{UCUM}, the \attention{Unified Code for Units of Measure}, spezifiziert, wie nicht-SI-Einheiten in SI-Einheiten repräsentiert werden können.

Die vielleicht \attention{wichtigste Klassifikation} stellt die \attention{International Statistical Classification of Diseases and Related Health Problems}, ICD, dar. 

Sie existiert seit 1893.
Die aktuell verwendete Version ist \textit{ICD-10}, bzw. \textit{ICD-10-GM}, wobei \textit{GM} für \enquote{German Modification} steht, um die Eigenheiten des deutschen Gesundheitssystems gerecht zu werden. 

Im Jahr 2014 wurde \textit{ICD-11} definiert, welche momentan eingeführt wird.
Dieser Vorgang wird allerdings noch mehrere Jahre dauern.

Die ICD Klassifikation ist wie folgt aufgebaut.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/03-icd.png}
  \caption{ICD Aufbau}
  \label{img-03-icd}
\end{figure}

Der Code besteht aus einem \enquote{Dreiteiler}, der hier in gelb dargestellt ist. 
Er beschreibt Kapitel und Kategorie der Krankheit.
Subkategorien verfeinern die Beschreibung des Krankheitsbilds (blau und hellblau).
Hat eine Erkrankung keine Unterkapitel, wird der ICD-Dreisteller alleinestehend notiert. 
Muss ein Krankheitsbild weiter verfeinert werden, wird nach dem Dreisteller ein \texttt{.-} notiert. 

Mit der ICD können auch Kausalitäten angegeben werden.
So wird die Ursache einer Diagnose mit einem Kreuz (\( + \)) notiert. 

Folgen eines Krankheitsbilds werden mit einenm Stern (\( * \)) versehen. 
Wird ein ICD Code mit \( * \) angegeben, \textit{MUSS} ein ICD Code mit (\( + \)) ebenfalls angegeben werden. 

Weitere Beschreibungen die nicht in die Kreuz- oder Sternkategorisierung passen, werden mit einem Ausrufezeichen (\( ! \)) notiert. 
Wird ein ICD Code mit \( ! \) angegeben, \textit{MUSS} ein ICD Code mit \( + \) oder \( * \) ebenfalls angegeben werden.  

Der \attention{Operations- und Prozedurenschlüssel} beschreibt eine abrechnungsrelevante Kodierung von Behandlungsmethoden. 
Er wird nicht mehr fortgeführt.

Neben diesen allgemeinen Klassifikationen gibt es auch krankheitsspezifische Klassifikationen, wie den \attention{New York Heart Association} Schlüssel. 
Der \textit{NYHA} spezifiziert Herzerkrankungen.

Die \attention{Pharmazentralnummer PZN} identifiziert ein Arzneimittel und seine Verpackung eindeutig. 
Wird ein minimales Detail an Arznei oder Verpackung verändert, zieht dies einen Neueintrag ins Register und eine neue PZN nach sich.

Eine ebenfalls auf Arzneimittel anzuwendende Klassifizierung ist die \attention{Anatomical Therapeutic Chemical Classification (ATC)}, welche den Wirkort und die Wirkweise eines Arzneimittels beschreibt. 

Eine wichtige Ontologie, welche als internationaler Standard anerkannt ist, ist \attention{SNOMED CT}.
SNOMED CT steht für \textit{Systematized Nomenclature of Medicine, Clinical Terms}. 
Neben der Verbindung von mehrern Bereichen definiert SNOMED CT eine Nomenklatur, sowie einen Thesaurus medizinischer Fachbegriffe.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{img/03-snomed.png}
  \caption{SNOMED-CT Beispiel}
  \label{img-03-snomed}
\end{figure}

Ist in Deutschland nicht lizenziert.

Es ist offensichtlich, dass es eine \attention{Vielzahl unterschiedlicher Systeme zur Klassifizierung medizinischer Fachbegriffe} gibt. 
Das \textit{UMLS (Unified Medical Language System)} versucht, diese Standards zu vereinheitlichen.
Es verbindet unter anderem SNOMED CT, ICD, LOINC. 

\subsection{Hilfe bei Dokumentation} 

Es kann von keinem Angestellten erwartet werden, dass z.B. alle ICD Codes auswendig gelernt werden. 
Daher benötigt man Hilfe bei der computergestützten Dokumentation von Krankheiten, Medikamentengaben und so weiter.

Ein zu komplexes System verringt die Anwenderakzeptanz und damit die Dokumentationsqualität.
Generell muss bei einer unterstützenden Anwendung auf viele Dinge geachtet werden:
\begin{itemize}
  \item Angemessenheit der Aufgaben
  \item Selbstbeschreibungfähigkeit
  \item Erwartungskonformität (sind Ergebnisse und Abläufe den Erwartungen entsprechend?)
  \item Erlernbarkeit
  \item Steuerbarkeit
  \item Robustheit
  \item Benutzerbindung
\end{itemize}

 \textit{Autocomplete} ist eines der mächtigsten Mittel, um Zeitaufwand und Fehlerrate beim Dokumentieren von Krankheiten und Medikamentengaben zu minimieren.
Falscher Einsatz dieser Technik kann allerdings zu einer höheren Fehlerrate führen, auch wenn andere Fehler gemacht werden als ohne Autocomplete.
