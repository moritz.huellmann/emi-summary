Benutzungsfehler können aus vielfältigen Gründen entstehen. 
Einige Bespiele sind unten stehend gelistet.
\begin{itemize}
  \item Eine benötigte Information ist nicht verfügbar.
  \item Eine nicht benötigte Information ist verfügbar.
  \item Ein Steuerelement wird anders dargestellt als allgemein üblich. 
  \item Ein Arbeitsschritt, der von einer Aufgabe erfordert ist, ist nicht möglich oder zu versteckt.
  \item Das System beendet die Aufgabenbearbeitung, wenn eine bestimmte Zeit keine neuen Eingaben gemacht wurden. 
  \item Dialoge müssen neugestartet werden, um Informationen korrigieren zu können.
  \item Das Tool kann nicht ohne externe Hilfe erlernt werden.
  \item  \dots
\end{itemize}

Es gibt offensichtlich noch viele andere Gründe. 
Im Allgemeinen kann aber ein \textit{Benutzungsfehler} durch viele Dinge ausgelöst werden, darunter zu wenig Informationen, zu viele Informationen, unergonomische Bedienoberfläche, umständlicher Arbeitsfluss, und so weiter. 

Das Verhalten eines Benutzers kann klassifiziert werden.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{img/08-gebrauchsarten.png}
  \caption{Gebrauchsarten Medizinischer Software}
  \label{img-08-gebrauchsarten}
\end{figure}

Dabei setzen sich die Anforderungen an die Gebrauchstauglichkeit nicht nur aus den Anforderungen der tatsächlichen Benutzer des Systems zusammen.
Es muss auch auf die Vorstellungen anderer \textit{Stakeholder} eingegangen werden. 
Diese erstellen ein \attention{Lastenheft}, welches Anforderungen an die Software definiert.
Von den Entwicklern der Software wird als Antwort darauf das \attention{Pflichtenheft} entworfen, welches mit konkreten Vorschlägen und Implementierungsdetails auf die im Lastenheft gelisteten Anforderungen eingeht. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{img/08-lasten-pflichten.png}
  \caption{Lasten- und Pflichtenheft}
  \label{img-img}
\end{figure}

Nach und \textit{auch während} der Fertigstellung der Software, muss diese evaluiert werden. Dies geschieht häufig nach dem V-Prinzip.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{img/08-validierung.png}
  \caption{Validierung von Arbeitsschritten}
  \label{img-img}
\end{figure}

Zu jedem Schritt der Entwicklung und Spezifikation gehört ein korrespondierender Test. 
Zwischen Phasen kann nicht nur in eine Richtung gewechselt werden. 
Es ist auch möglich bei entdeckten Fehlern auf diese zu reagieren und z.B. neue Systemanforderungen zu definieren, welche auch getestet werden müssen.

Bei der \attention{Usability Evaluation}, also einer Nutzerstudie, muss darauf geachtet werden, dass der Interviewer kein Wissen preisgibt, dass der Interviewte nicht besitzt. 
So können Fehler in der Gestaltung der UI entdeckt werden und auch fehlerhafte Arbeitsabläufe verbessert werden.
Generell ist zwischen \attention{geschlossenen Fragen} und \attention{offenen Fragen} zu unterscheiden. 
Eine \textit{geschlossene Frage} hat fest definierte Antwortmöglichkeiten, während eine \textit{offene Frage} eine Elaboration des Interviewten erfordert. 
Evaluationen werden meist in \attention{Kontextuellen Interviews} durchgeführt, also an dem Ort, wo das System später auch eingesetzt werden soll.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{img/08-anforderungsherleitung.png}
  \caption{Herleitung von Anforderungen}
  \label{img-08-anforderungsherleitung}
\end{figure}

Die Herleitung von Anforderungen basiert auf der Identifikation von \attention{Erfordernissen}.

\begin{defi}{Erfordernis}
  Erfordernisse sind Informationen, die unabhängig von der Person, in einem bestimmten Kontext verfügbar sein müssen, um eine ordnungsgemäße Verwendung des Produkts sicherstellen zu können.
  Diese Informationen können auch in Form von \textit{Wissen} der Benutzer vorhanden sein.
\end{defi}

\begin{defi}{Anforderung}
  Aus einer Erfordernis ergibt sich direkt eine Anforderung an das System. 
  Muss eine Information sichtbar/abrufbar sein, muss das System dies ermöglichen. 
  Müssen Informationen eingebene/verändert/gelöscht werden, muss das System Mechanismen dafür bereitstellen. 
\end{defi}

Sind die Erfordernisse in Anforderungen umgesetzt und diese Implementiert worden, muss das System final auf seine Tauglichkeit geprüft werden.
Dazu zählt nicht nur die \textit{Usability Evaluation}, welche die Benutzerfreundlichkeit eines Systems untersucht, sondern auch \attention{Validierung} und \attention{Verifikation}.

\begin{alertbox}
  Validierung und Verifikation sind unterschiedliche Konzepte und müssen auf jeden Fall unterschieden werden können.
\end{alertbox}

\begin{defi}{Validierung}
  \begin{center}
    \enquote{Bestätigung durch objektiven Nachweis, dass die Anforderungen für eine bestimmte Anwendung erfüllt sind.} 
  \end{center}
  \tcblower
  Ist es das richtige System?
\end{defi}

\begin{defi}{Verifikation}
  \begin{center}
    \enquote{Prüfung mit objektiven Mitteln, dass spezifische (Produkt-)Eigenschaften erfüllt sind.}
  \end{center}
  \tcblower
  Arbeitet das System richtig?
\end{defi}

Wir werden nun einen genaueren Blick auf die \textit{Usability Evaluation} werfen. 
Selbstverständlich ist auch \attention{Usability} formal definiert.

\begin{defi}{Usability}
  Ausmaß, in dem ein System, ein Produkt oder eine Dienstleistung durch bestimmte Benutzer in einem bestimmten Nutzungskontext genutzt werden kann, um bestimmte Ziele effektiv, effizient und zufriedenstellend zu erreichen.
\end{defi}

Für Medizinprodukte müssen mehrere auf \textit{Usability} ausgelegte Evaluationen durchgeführt werden.
\begin{description}
  \item[Formative Evaluation] – UI \textit{Bewertung}; Stärken und Schwächen ermitteln, sowie Fehler erkennen.
  \item[Summative Evaluation] – UI \textit{Evaluation}; Objektiver Nachweis, dass die UI sicher bedient werden kann.
  \item[Usability Test] – UI \textit{Beurteilung} in vorgesehener Umgebung mit vorgesehenen Usern.
\end{description}

Durch diese Evaluationen können \attention{Systeme untereinander vergleichen}, \attention{bewertet} und \attention{analysiert}.

Evalutionsmethoden sind in \attention{analytisch} und \attention{empirisch} zu teilen.

Eine Möglichkeit der \textit{analytischen Evaluation} ist die \attention{heuristische}. 
Diese beschreibt das Untersuchen des Systems durch einen Experten im Usability-Bereich. 
Dieser Experte geht bei seiner Beurteilung heuristisch, also nach einem festen Regelwerk, vor.
So können generische UI Fehler schnell erkannt werden, ein echter Nutzer ist aber so nicht in den Test involviert. 
Die Regeln, nach denen evaluiert wird, sind häufig die nach Nielsen.

\begin{defi}{Heuristische Evaluation nach Nielsen}
  \begin{description}
    \item[Sichtbarkeit des Systemzustandes] – Nutzer muss \textit{immer} wissen, wo im System und Prozess er sich befindet.
    \item[Übereinstimmung System und realer Welt] – Systemsprache sollte gesprochene Sprache sein. Keine Unkenntlichmachung durch unbekannte Codes.
    \item[Nutzerkontrolle und Freiheit] – Fehler sollten nach Erkennung rückgängig gemacht werden können. Menüs sollten in beide Richtungen durchlaufbar sein und abgebrochen werden können.
    \item[Konsistenz und Standards] – Innerhalb der UI konstante Platzierung von Knöpfen, Suchleiten etc. Benennung gleicher Sachverhalte immer gleich.
    \item[Fehlerprävention] – Eingabeüberprüfung sollte durchgeführt werden. Nicht verfügbare Buttons müssen deaktiviert werden etc.
    \item[Erkennen > Erinnern] – Benutzung muss intuitiv sein, sodass sich kein Ablauf gemerkt werden muss.
    \item[Flexibilität und Effizienz der Nutzung] – Icons statt Text, Verfügbarkeit von Shortcuts.
    \item[Ästhetik und minimales Design] – Verhinder visuelle Überforderung.
    \item[Generelle Hilfe für den Anwender] – Fehlermeldungen gut verständlich, Informationen immer abrufbereit wenn sie gebraucht werden, Anleitungen verfügbar. Ursache von Fehlern präzise darstellen.
    \item[Dokumentation] – System sollte ohne externe Hilfe bedient werden können. Daher für Anwender verständliche Dokumentation des Gebrauchsprozesses.
  \end{description}
\end{defi}

Selbstverständlich gibt es auch eine ISO Norm für Usability.

\begin{defi}{ISO 9231-110}
  Es ist beim Design von medizinischer Software auf folgendes zu achten.
  \begin{enumerate}
    \item Aufgabenangemessenheit. (Dialog, wenn er Aufgaben unterstützt und nicht behindert)
    \item Selbstbeschreibungfähigkeit. (Unmittelbare Verständlichkeit von Fehlermeldungen)
    \item Erwartungskonformität.
    \item Erlernbarkeit.
    \item Steuerbarkeit. (Dialog, der in alle Richtungen durchlaufbar ist)
    \item Robustheit gegen Benutzerfehler.
    \item Benutzerbindung.
  \end{enumerate}
\end{defi}

Ist noch kein konkretes Produkt verfügbar, kann ein \textit{Cognitive Walkthrough} durchgeführt werden, in dem das System nur in der eigenen Vorstellung bedient wird. 

Beim Testen der Software muss auf Gefühlsregungen, Gesichtsausdrücken, Gestik und Gesprochenes geachtet werden. 
Metriken wie Zeit bis Aufgabenerfüllung,  Grad der selbstständigen Durchführung (vollständig/unvollständig/vollständig mit Hilfe/etc) müssen protokolliert werden.

Risikomatrix: Häufigkeit und Ausmaß. 

